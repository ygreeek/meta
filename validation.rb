module Validation
  module ClassMethods
    class << self; attr_accessor :validations; end
    self.validations = []

    ALLOWED_TYPES = %i[presence format type]

    def validate(name, type, params = nil)
      if ALLOWED_TYPES.include? type
        Validation::ClassMethods.validations.push [name, type, params]
      else
        raise ArgumentError, "Invalid validation type: #{type}"
      end
    end
  end

  module InstanceMethods
    def validate!
      errors = []
      validations = Validation::ClassMethods.validations

      validations.each do |validation|
        variable = instance_variable_get("@#{validation.first}".to_sym)
        type = validation[1]
        params = validation[2]

        if type == :presence
          errors << 'Nil or empty string' if variable.nil? || variable.empty?
        end

        if type == :format
          errors << 'Invalid format' unless variable&.match params
        end

        if type == :type
          errors << 'Invalid class type' unless variable.is_a? params
        end
      end

      errors
    end

    def valid?
      validate!.empty?
    end
  end

  def self.included(base)
    base.extend ClassMethods
    base.include InstanceMethods
  end
end

class Test
  include Validation

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  validate :name, :presence
  validate :name, :type, String
  validate :name, :format, /\At/
end
