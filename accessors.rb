module MyattrAccessor
  def attr_accessor_with_history(*names)
    names.each do |name|
      var_name = "@#{name}".to_sym
      variable_values = []

      define_method(name) { instance_variable_get(var_name) }
      define_method("#{name}=".to_sym) do |value|
        instance_variable_set(var_name, value)
        variable_values.push instance_variable_get(var_name)
      end
      define_method("#{name}_history") { variable_values }
    end
  end

  def strong_attr_accessor(name, klass)
    var_name = "@#{name}".to_sym
    define_method(name) { instance_variable_get(var_name) }
    define_method("#{name}=".to_sym) do |value|
      if value.is_a? klass
        instance_variable_set(var_name, value)
      else
        raise TypeError, 'Type mismatch'
      end
    end
  end
end

class Test
  extend MyattrAccessor

  attr_accessor_with_history :a, :b, :c

  strong_attr_accessor :d, Integer
end
